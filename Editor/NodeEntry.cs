﻿namespace CodeExplorer
{
    public struct NodeEntry
    {
        public static NodeEntry Separator { get; set; }

        public string Text { get; set; }
        public bool IsUnderlined { get; set; }

        public NodeEntry(string text, bool isUnderlined = false)
        {
            Text = text;
            IsUnderlined = isUnderlined;
        }
    }
}
