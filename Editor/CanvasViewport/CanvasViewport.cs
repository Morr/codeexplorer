using UnityEngine;
using System.Collections;
using System;
using UnityEditor;

namespace CodeExplorer
{
    // Modified version of EditorZoomer https://gist.github.com/MattRix/564fa9c36c511ce9ec2b8f5c84022a97
    public class CanvasViewport
    {
        private const string VIEWPORT_SCALE_PREFS_KEY = "ViewportScale";
        private const string VIEWPORT_POSITION_X_PREFS_KEY = "ViewportPositionX";
        private const string VIEWPORT_POSITION_Y_PREFS_KEY = "ViewportPositionY";
        private const float EDITOR_WINDOW_TAB_HEIGHT = 21.0f;
        private const float MAX_CANVAS_SIZE = 10000.0f;

        public float ViewportScale { get; private set; } = 1.0f;
        private Vector2 ViewportPosition { get; set; }
        private Rect CanvasArea { get; set; }
        private Vector2 LastMousePosition { get; set; } = Vector2.zero;
        private Matrix4x4 DefaultMatrix { get; set; }

        public CanvasViewport()
        {
            RestoreViewportTransform();
        }

        public Rect Begin(params GUILayoutOption[] options)
        {
            //fill the available area
            var possibleCanvasArea = GUILayoutUtility.GetRect(0, MAX_CANVAS_SIZE, 0, MAX_CANVAS_SIZE, options);

            if (Event.current.type == EventType.Repaint) //the size is correct during repaint, during layout it's 1,1
            {
                CanvasArea = possibleCanvasArea;
            }

            GUI.EndGroup(); // End the group Unity begins automatically for an EditorWindow to clip out the window tab. This allows us to draw outside of the size of the EditorWindow.

            Rect clippedArea = CanvasArea.ScaleSizeBy(1f / ViewportScale, CanvasArea.TopLeft());
            clippedArea.y += EDITOR_WINDOW_TAB_HEIGHT;
            GUI.BeginGroup(clippedArea);

            DefaultMatrix = GUI.matrix;
            Matrix4x4 translation = Matrix4x4.TRS(clippedArea.TopLeft(), Quaternion.identity, Vector3.one);
            Matrix4x4 scale = Matrix4x4.Scale(new Vector3(ViewportScale, ViewportScale, 1.0f));
            GUI.matrix = translation * scale * translation.inverse * GUI.matrix;

            return clippedArea;
        }

        public void End()
        {
            GUI.matrix = DefaultMatrix;
            GUI.EndGroup();
            GUI.BeginGroup(new Rect(0.0f, EDITOR_WINDOW_TAB_HEIGHT, Screen.width, Screen.height));
        }

        public Vector2 GetTransformedMousePosition(Vector2 mousePosition)
        {
            Vector2 transformedMousePosition = mousePosition / ViewportScale + GetContentOffset();
            return transformedMousePosition;
        }

        public Vector2 GetContentOffset()
        {
            Vector2 offset = -ViewportPosition / ViewportScale; //offset the midpoint
            offset -= (CanvasArea.size / 2f) / ViewportScale; //offset the center
            return offset;
        }

        public void ResetViewport()
        {
            ViewportScale = 1.0f;
            ViewportPosition = Vector2.zero;
            SaveViewportTransform();
        }

        public void SaveViewportTransform()
        {
            EditorPrefs.SetFloat(VIEWPORT_SCALE_PREFS_KEY, ViewportScale);
            EditorPrefs.SetFloat(VIEWPORT_POSITION_X_PREFS_KEY, ViewportPosition.x);
            EditorPrefs.SetFloat(VIEWPORT_POSITION_Y_PREFS_KEY, ViewportPosition.y);
        }

        private void RestoreViewportTransform()
        {
            ViewportScale = EditorPrefs.GetFloat(VIEWPORT_SCALE_PREFS_KEY, ViewportScale);
            float x = EditorPrefs.GetFloat(VIEWPORT_POSITION_X_PREFS_KEY, ViewportPosition.x);
            float y = EditorPrefs.GetFloat(VIEWPORT_POSITION_Y_PREFS_KEY, ViewportPosition.y);
            ViewportPosition = new Vector2(x, y);
        }

        public void ProcessEvents(Event e)
        {
            ProcessMouseEvents(e);
            ProcessScrollEvents(e);
        }

        private void ProcessMouseEvents(Event e)
        {
            if (e.isMouse == true)
            {
                if (e.type == EventType.MouseDrag && e.button == 0)
                {
                    var mouseDelta = Event.current.mousePosition - LastMousePosition;

                    ViewportPosition += mouseDelta;
                    e.Use();
                }

                if (e.type == EventType.MouseUp && e.button == 0)
                {
                    SaveViewportTransform();
                }

                LastMousePosition = e.mousePosition;
            }
        }

        private void ProcessScrollEvents(Event e)
        {
            if (e.type == EventType.ScrollWheel)
            {
                float previousScale = ViewportScale;
                float scaleChange = 1.10f;

                ViewportScale *= Mathf.Pow(scaleChange, -e.delta.y / 3f);
                ViewportScale = Mathf.Clamp(ViewportScale, 0.1f, 10f);
                SaveViewportTransform();

                bool shouldZoomTowardsMouse = true; //if this is false, it will always zoom towards the center of the content (0,0)

                if (shouldZoomTowardsMouse == true)
                {
                    //we want the same content that was under the mouse pre-zoom to be there post-zoom as well
                    //in other words, the content's position *relative to the mouse* should not change

                    Vector2 areaMousePos = e.mousePosition - CanvasArea.center;

                    Vector2 contentOldMousePos = (areaMousePos / previousScale) - (ViewportPosition / previousScale);
                    Vector2 contentMousePos = (areaMousePos / ViewportScale) - (ViewportPosition / ViewportScale);

                    Vector2 mouseDelta = contentMousePos - contentOldMousePos;

                    ViewportPosition += mouseDelta * ViewportScale;
                }

                e.Use();
            }
        }
    }
}

