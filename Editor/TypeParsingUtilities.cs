using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace CodeExplorer
{
    public class TypeParsingUtilities : MonoBehaviour
    {
        private const string PUBLIC_LITERAL = "+";
        private const string NON_PUBLIC_LITERAL = "-";
        private const BindingFlags BINDING_FLAGS = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;

        public static Stereotype GetStereotype(Type type)
        {
            if (type.IsValueType == true)
            {
                if (type.IsEnum == true)
                {
                    return Stereotype.Enum;
                }
                else
                {
                    return Stereotype.Struct;
                }
            }
            else if (type.IsInterface == true)
            {
                return Stereotype.Interface;
            }
            else
            {
                return Stereotype.Class;
            }
        }

        public static List<NodeEntry> CreateEnumEntries(Type type)
        {
            List<NodeEntry> entries = new List<NodeEntry>();
            entries.Add(NodeEntry.Separator);

            Array values = Enum.GetValues(type);

            for (int i = 0; i < values.Length; i++)
            {
                entries.Add(new NodeEntry(values.GetValue(i).ToString()));
            }

            return entries;
        }

        public static List<NodeEntry> CreateNonEnumEntries(Type type)
        {
            List<NodeEntry> entries = new List<NodeEntry>();
            CreateFieldsEntries(type, entries);
            CreatePropertiesEntries(type, entries);
            CreateMethodsEntries(type, entries);
            return entries;
        }

        private static void CreateFieldsEntries(Type type, List<NodeEntry> entries)
        {
            FieldInfo[] fields = type.GetFields(BINDING_FLAGS);

            for (int i = 0; i < fields.Length; i++)
            {
                FieldInfo field = fields[i];

                if (IsCompilerGenerated(field) == false && IsMemberNameValid(field) == true)
                {
                    string entry = $"{GetVisibilityText(field.IsPublic)} {field.Name} : <color={NodeStyles.TYPE_COLOR}>{field.FieldType.Name}</color>";
                    entries.Add(new NodeEntry(entry, field.IsStatic));
                }
            }
        }

        private static void CreatePropertiesEntries(Type type, List<NodeEntry> entries)
        {
            PropertyInfo[] properties = type.GetProperties(BINDING_FLAGS);

            for (int i = 0; i < properties.Length; i++)
            {
                PropertyInfo property = properties[i];
                MethodInfo getter = property.GetGetMethod();
                MethodInfo setter = property.GetSetMethod();
                bool isPublic = false;
                bool isStatic = false;

                if (getter != null)
                {
                    isPublic = getter.IsPublic;
                    isStatic = getter.IsStatic;
                }

                if (setter != null)
                {
                    isPublic = setter.IsPublic;
                    isStatic = setter.IsStatic;
                }

                if (IsMemberNameValid(property) == true)
                {
                    string entry = $"{GetVisibilityText(isPublic)} {property.Name} : <color={NodeStyles.TYPE_COLOR}>{property.PropertyType.Name}</color>";
                    entries.Add(new NodeEntry(entry, isStatic));
                }
            }

            if (entries.Count > 0)
            {
                entries.Insert(0, NodeEntry.Separator);
            }
        }

        private static void CreateMethodsEntries(Type type, List<NodeEntry> entries)
        {
            MethodInfo[] methods = type.GetMethods(BINDING_FLAGS);
            int lastEntriesCount = entries.Count;

            for (int i = 0; i < methods.Length; i++)
            {
                MethodInfo method = methods[i];

                ParameterInfo[] parameters = method.GetParameters();
                string[] parametersTypesNames = new string[parameters.Length];

                for (int parameterIndex = 0; parameterIndex < parameters.Length; parameterIndex++)
                {
                    parametersTypesNames[parameterIndex] = $"<color={NodeStyles.TYPE_COLOR}>{parameters[parameterIndex].ParameterType.Name}</color>";
                }

                string parametersString = string.Join(", ", parametersTypesNames);

                if (IsCompilerGenerated(method) == false && IsMemberNameValid(method) == true)
                {
                    string entry = $"{GetVisibilityText(method.IsPublic)} {method.Name}({parametersString}) : <color={NodeStyles.TYPE_COLOR}>{method.ReturnType.Name}</color>";
                    entries.Add(new NodeEntry(entry, method.IsStatic));
                }
            }

            if (entries.Count > lastEntriesCount)
            {
                entries.Insert(lastEntriesCount, NodeEntry.Separator);
            }
        }

        private static string GetVisibilityText(bool isPublic)
        {
            string visibilityText = isPublic == true ? PUBLIC_LITERAL : NON_PUBLIC_LITERAL;
            visibilityText = $"<color={NodeStyles.TYPE_COLOR}>{visibilityText}</color>";
            return visibilityText;
        }

        private static bool IsCompilerGenerated(MemberInfo member)
        {
            CompilerGeneratedAttribute compilerGeneratedAttribute = member.GetCustomAttribute<CompilerGeneratedAttribute>();
            return compilerGeneratedAttribute != null;
        }

        private static bool IsMemberNameValid(MemberInfo member)
        {
            return CodeGenerator.IsValidLanguageIndependentIdentifier(member.Name);
        }
    }
}
