using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace CodeExplorer
{
    // Based on https://gram.gs/gramlog/creating-node-based-editor-unity/
    public class CodeExplorerWindow : EditorWindow
    {
        private const string WINDOW_TITLE = "Code Explorer";
        private const string NONE_NAMESPACE_DISPLAY_STRING = "None";
        private const string SELECTED_NAMESPACE_PREFS_KEY = "SelectedNamespace";

        private static CodeExplorerWindow Instance { get; set; }

        private TypesDataCollector DataCollector { get; set; }

        private List<Node> Nodes { get; set; } = new List<Node>();
        private bool IsNodesGUIInitialized { get; set; }
        private NodesDataStorage NodesDataStorage { get; set; }

        private string[] NamespacesNames { get; set; }
        private int SelectedNamespaceIndex { get; set; }

        private Vector2 ContentOffset { get; set; }
        private CanvasViewport CanvasViewport { get; set; }

        [MenuItem("Window/Code Explorer _`")]
        public static void OpenWindow()
        {
            if (Instance == null)
            {
                CodeExplorerWindow window = GetWindow<CodeExplorerWindow>(WINDOW_TITLE);
            }
            else
            {
                Instance.Close();
            }
        }

        private void OnEnable()
        {
            Initialize();
        }

        private void Initialize()
        {
            Instance = this;
            CanvasViewport = new CanvasViewport();
            DataCollector = new TypesDataCollector();
            NamespacesNames = new string[DataCollector.Namespaces.Count];
            DataCollector.Namespaces.CopyTo(NamespacesNames);

            for (int i = 0; i < NamespacesNames.Length; i++)
            {
                if (NamespacesNames[i] == null)
                {
                    NamespacesNames[i] = NONE_NAMESPACE_DISPLAY_STRING;
                }
            }

            SelectedNamespaceIndex = EditorPrefs.GetInt(SELECTED_NAMESPACE_PREFS_KEY, 0);
            
            if (SelectedNamespaceIndex >= NamespacesNames.Length)
            {
                SelectedNamespaceIndex = 0;
                StoreSelectedNamespaceIndex();
            }

            NodesDataStorage = GetNodesDataStorage();
            NodesDataStorage.CleanNodeData();
            InstantiateNodesForNamespace(NamespacesNames[SelectedNamespaceIndex]);
        }

        private void OnGUI()
        {
            if (IsNodesGUIInitialized == false)
            {
                for (int i = 0; i < Nodes.Count; i++)
                {
                    Nodes[i].InitializeGUI();
                }

                IsNodesGUIInitialized = true;
            }

            Event e = Event.current;
            ProcessNodeEvents(e);
            ProcessWindowEvents(e);
            CanvasViewport.ProcessEvents(e);

            int previousSelectedNamespaceIndex = SelectedNamespaceIndex;
            SelectedNamespaceIndex = EditorGUI.Popup(new Rect(0.0f, 0.0f, position.width, 18.0f), SelectedNamespaceIndex, NamespacesNames);

            if (previousSelectedNamespaceIndex != SelectedNamespaceIndex)
            {
                StoreSelectedNamespaceIndex();
                string selectedNamespaceName = NamespacesNames[SelectedNamespaceIndex];
                InstantiateNodesForNamespace(selectedNamespaceName);
            }

            DrawCanvas();

            if (GUI.changed == true)
            {
                Repaint();
            }
        }

        private void StoreSelectedNamespaceIndex()
        {
            EditorPrefs.SetInt(SELECTED_NAMESPACE_PREFS_KEY, SelectedNamespaceIndex);
        }

        private void ProcessWindowEvents(Event e)
        {
            if (e.isKey == true && e.keyCode == KeyCode.Space)
            {
                CanvasViewport.ResetViewport();
                GUI.changed = true;
            }
        }

        private void DrawCanvas()
        {
            Rect canvasRect = position;
            canvasRect.position -= Vector2.down * 5.0f;

            if (GUI.Button(canvasRect, "", GUIStyle.none))
            {
                GUI.FocusControl(null);
            }

            CanvasViewport.Begin();
            DrawTypeNodes();
            CanvasViewport.End();
            ContentOffset = CanvasViewport.GetContentOffset();
        }

        private void InstantiateNodesForNamespace(string namespaceName)
        {
            IsNodesGUIInitialized = false;
            Nodes.Clear();

            Vector2 defaultNodePosition = new Vector2(50, 50);

            if (namespaceName.Equals(NONE_NAMESPACE_DISPLAY_STRING) == true)
            {
                namespaceName = null;
            }

            foreach (KeyValuePair<Type, int> pair in DataCollector.TypeScriptAssetInstanceIdMap) 
            {
                Type type = pair.Key;
                int scriptAssetInstanceId = pair.Value;

                if (type.Namespace == namespaceName)
                {
                    Node node;

                    if (NodesDataStorage.TryGetNodeData(scriptAssetInstanceId, out NodesDataStorage.SerializedNodeData data) == true)
                    {
                        node = new Node(type, scriptAssetInstanceId, data.NodeCanvasPosition);
                    }
                    else
                    {
                        node = new Node(type, scriptAssetInstanceId, defaultNodePosition);
                        defaultNodePosition += Vector2.right * (NodeStyles.MIN_NODE_WIDTH + NodeStyles.DEFAULT_NODES_HORIZONTAL_SPACING);
                    }

                    node.OnDragFinished += StoreNodeData;
                    Nodes.Add(node);
                }
            }
        }

        private void DrawTypeNodes()
        {
            if (Nodes != null)
            {
                for (int i = 0; i < Nodes.Count; i++)
                {
                    Nodes[i].Draw(ContentOffset);
                }
            }
        }

        private void ProcessNodeEvents(Event e)
        {
            if (Nodes != null)
            {
                Vector2 eventMousePosition = e.mousePosition;
                Vector2 eventMouseDelta = e.delta;
                e.mousePosition = CanvasViewport.GetTransformedMousePosition(eventMousePosition);
                e.delta /= CanvasViewport.ViewportScale;

                Node lastModifiedNode = null;

                for (int i = Nodes.Count - 1; i >= 0; i--)
                {
                    Node node = Nodes[i];
                    bool isGUIChanged = node.ProcessEvents(e);

                    if (isGUIChanged == true)
                    {
                        GUI.changed = true;
                        lastModifiedNode = node;
                    }
                }

                // Fix display order
                if (lastModifiedNode != null)
                {
                    Nodes.Remove(lastModifiedNode);
                    Nodes.Add(lastModifiedNode);
                }

                e.mousePosition = eventMousePosition;
                e.delta = eventMouseDelta;
            }
        }

        private NodesDataStorage GetNodesDataStorage()
        {
            NodesDataStorage[] nodesMetaDataInstances = ScriptableObjectHelpers.GetAllInstances<NodesDataStorage>();

            if (nodesMetaDataInstances.Length > 0)
            {
                return nodesMetaDataInstances[0];
            }
            else
            {
                return ScriptableObjectHelpers.CreateScriptableObjectAsset<NodesDataStorage>();
            }
        }

        private void StoreNodeData(Node node)
        {
            NodesDataStorage.StoreNodeData(node);
        }
    }
}
