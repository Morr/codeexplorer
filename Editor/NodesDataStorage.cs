using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CodeExplorer
{
    public class NodesDataStorage : ScriptableObject
    {
        [field: SerializeField]
        private List<SerializedNodeData> SerializedNodesData { get; set; }

        public bool IsCleared { get; private set; }

        private Dictionary<int, int> StorageIndicesDictionary { get; set; }

        public void StoreNodeData(Node node)
        {
            InitializeStorageIndicesMap();

            if (StorageIndicesDictionary.TryGetValue(node.ScriptAssetInstanceId, out int storageIndex) == true)
            {
                SerializedNodesData[storageIndex] = new SerializedNodeData(node);
            }
            else
            {
                SerializedNodesData.Add(new SerializedNodeData(node));
                StorageIndicesDictionary.Add(node.ScriptAssetInstanceId, SerializedNodesData.Count - 1);
            }

            EditorUtility.SetDirty(this);
        }

        public bool TryGetNodeData(int scriptAssetInstanceId, out SerializedNodeData data)
        {
            InitializeStorageIndicesMap();

            if (StorageIndicesDictionary.TryGetValue(scriptAssetInstanceId, out int storageIndex))
            {
                data = SerializedNodesData[storageIndex];
                return true;
            }

            data = default;
            return false;
        } 

        public void CleanNodeData()
        {
            if (IsCleared == false && SerializedNodesData != null)
            {
                SerializedNodesData.RemoveAll((nodeData) => EditorUtility.InstanceIDToObject(nodeData.ScriptAssetInstanceId) == null);
                InitializeStorageIndicesMap(true);
                EditorUtility.SetDirty(this);
                IsCleared = true;
            }
        }

        private void InitializeStorageIndicesMap(bool isCurrentDictioanryDiscarded = false)
        {
            if (StorageIndicesDictionary == null || isCurrentDictioanryDiscarded == true)
            {
                StorageIndicesDictionary = new Dictionary<int, int>();

                for (int i = 0; i < SerializedNodesData.Count; i++)
                {
                    StorageIndicesDictionary.Add(SerializedNodesData[i].ScriptAssetInstanceId, i);
                }
            }
        }

        [System.Serializable]
        public struct SerializedNodeData
        {
            [field: SerializeField]
            public int ScriptAssetInstanceId { get; set; }
            [field: SerializeField]
            public Vector2 NodeCanvasPosition { get; set; }

            public SerializedNodeData(Node node)
            {
                ScriptAssetInstanceId = node.ScriptAssetInstanceId;
                NodeCanvasPosition = node.PositionOnCanvas;
            }
        }
    }
} 

