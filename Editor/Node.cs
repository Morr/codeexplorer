using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;

namespace CodeExplorer
{
    public partial class Node
    {
        public Action<Node> OnDragStarted { get; set; } = delegate { };
        public Action<Node> OnDragFinished { get; set; } = delegate { };
        public Vector2 PositionOnCanvas { get; set; }
        public int ScriptAssetInstanceId { get; set; }

        private float ContentHeight { get; set; }
        private float ContentWidth { get; set; }
        private bool IsDragged { get; set; }

        private string CahcedTypeName { get; set; }
        private Stereotype CachedStereotype { get; set; }
        private List<NodeEntry> CachedEntries { get; set; }

        public Node(Type type, int scriptAssetInstanceId, Vector2 position)
        {
            CahcedTypeName = type.Name;
            ScriptAssetInstanceId = scriptAssetInstanceId;
            Stereotype stereotype = TypeParsingUtilities.GetStereotype(type);
            CachedStereotype = stereotype;

            switch (stereotype)
            {
                case Stereotype.Enum:
                    CachedEntries = TypeParsingUtilities.CreateEnumEntries(type);
                    break;
                default:
                    CachedEntries = TypeParsingUtilities.CreateNonEnumEntries(type);
                    break;
            }

            PositionOnCanvas = position;
            GetNodeRect();
        }

        public void InitializeGUI()
        {
            RecalculateContentWidth();
        }

        public void Draw(Vector2 positionOffset)
        {
            Rect nodeRect = GetNodeRect();
            nodeRect.position -= positionOffset;

            Rect drawRect = GetDrawRect(nodeRect);

            DrawBackground(nodeRect);
            DrawHeader(ref drawRect);
            DrawEntries(ref drawRect);

            ContentHeight = (drawRect.position.y - nodeRect.position.y) + drawRect.height;
            GetNodeRect();
        }

        public bool ProcessEvents(Event e)
        {
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        Rect nodeRect = GetNodeRect();
                        if (nodeRect.Contains(e.mousePosition) == true)
                        {
                            if (e.clickCount == 1)
                            {
                                IsDragged = true;
                                OnDragStarted.Invoke(this);

                                GUI.changed = true;
                            }
                            else if (e.clickCount == 2)
                            {
                                AssetDatabase.OpenAsset(ScriptAssetInstanceId);
                            }
                        }

                        GUI.changed = true;
                        return true;
                    }

                    break;

                case EventType.MouseUp:
                    if (IsDragged == true)
                    {
                        IsDragged = false;
                        OnDragFinished.Invoke(this);
                    }

                    break;

                case EventType.MouseDrag:
                    if (e.button == 0 && IsDragged == true)
                    {
                        Drag(e.delta);
                        e.Use();
                        return true;
                    }

                    break;
            }

            return false;
        }

        private void Drag(Vector2 delta)
        {
            PositionOnCanvas += delta;
        }

        private void DrawBackground(Rect backgroundRect)
        {
            GUI.Box(backgroundRect, string.Empty, NodeStyles.NodeBoxStyle);

            //Vector3[] corners = new Vector3[]
            //{
            //    backgroundRect.position,
            //    backgroundRect.position + new Vector2(backgroundRect.width, 0.0f),
            //    backgroundRect.position + new Vector2(backgroundRect.width, backgroundRect.height),
            //    backgroundRect.position + new Vector2(0.0f, backgroundRect.height),
            //    backgroundRect.position
            //};

            //Handles.color = Color.white * 0.5f;
            //Handles.DrawPolyLine(corners);
        }

        private void DrawHeader(ref Rect drawRect)
        {
            DrawSpace(ref drawRect, 10.0f);

            if (CachedStereotype != Stereotype.Class)
            {
                DrawLabel(ref drawRect, $"<color={NodeStyles.TYPE_COLOR}><<{CachedStereotype}>></color>", 16.0f, NodeStyles.NodeStereotypeStyle);
            }
            else
            {
                DrawSpace(ref drawRect, 5.0f);
            }

            DrawLabel(ref drawRect, CahcedTypeName, 26.0f, NodeStyles.NodeTitleStyle);
        }

        private void DrawEntries(ref Rect drawRect)
        {
            if (CachedEntries == null)
            {
                return;
            }

            string longestEntry = string.Empty;

            for (int i = 0; i < CachedEntries.Count; i++)
            {
                string text = CachedEntries[i].Text;

                if (text != null)
                {
                    DrawLabel(ref drawRect, text, 20.0f, NodeStyles.NodeEntryStyle, CachedEntries[i].IsUnderlined);
                }
                else
                {
                    DrawSeparator(ref drawRect);
                }
            }
        }

        private void DrawLabel(ref Rect drawRect, string text, float height, GUIStyle style, bool isUnderlined = false)
        {
            if (isUnderlined == true)
            {
                float labelWidth = CalculateLabelWidth(text, NodeStyles.NodeEntryStyle);
                Vector2 leftPoint = drawRect.position + Vector2.up * 18.0f;
                Vector2 rightPoint = leftPoint + Vector2.right * labelWidth;
                leftPoint += Vector2.right * NodeStyles.NODE_ENTRY_LEFT_PADDING;

                Handles.color = style.normal.textColor;
                Handles.DrawLine(leftPoint, rightPoint);
            }

            drawRect.height = height;
            GUI.Label(drawRect, text, style);
            drawRect.position += Vector2.up * height;
        }

        private void DrawSpace(ref Rect drawRect, float height)
        {
            drawRect.position += Vector2.up * height;
        }

        private void DrawSeparator(ref Rect drawRect)
        {
            Handles.color = Color.gray;
            float halfSeparatorHeight = NodeStyles.NODE_SEPARATOR_HEIGHT * 0.5f;
            drawRect.position += Vector2.up * halfSeparatorHeight;
            Handles.DrawLine(drawRect.position, drawRect.position + Vector2.right * drawRect.width);
            drawRect.position += Vector2.up * halfSeparatorHeight;
        }

        private Rect GetNodeRect()
        {
            float width = Mathf.Max(NodeStyles.MIN_NODE_WIDTH, ContentWidth + NodeStyles.NODE_BACKGROUND_OFFSET * 2.0f);
            Vector2 size = new Vector2(width, ContentHeight);
            return new Rect(PositionOnCanvas, size);
        }

        private Rect GetDrawRect(Rect backgroundRect)
        {
            Vector2 size = new Vector2(backgroundRect.width - NodeStyles.NODE_BACKGROUND_OFFSET * 2.0f, 0.0f);
            Vector2 position = backgroundRect.position + Vector2.right * NodeStyles.NODE_BACKGROUND_OFFSET;
            return new Rect(position, size);
        }

        private void RecalculateContentWidth()
        {
            ContentWidth = CalculateLabelWidth(CahcedTypeName, NodeStyles.NodeTitleStyle) + NodeStyles.NODE_TITLE_HORIZONTAL_PADDING * 2.0f;
            string longestEntry = string.Empty;

            for (int i = 0; i < CachedEntries.Count; i++)
            {
                string text = CachedEntries[i].Text;

                if (text != null && text.Length > longestEntry.Length)
                {
                    longestEntry = text;
                }
            }

            if (longestEntry != null)
            {
                float longestEntryTotalWidth = CalculateLabelWidth(longestEntry, NodeStyles.NodeEntryStyle) + NodeStyles.NODE_ENTRY_LEFT_PADDING;
                ContentWidth = Mathf.Max(ContentWidth, longestEntryTotalWidth);
            }
        }

        private float CalculateLabelWidth(string labelText, GUIStyle style)
        {
            return style.CalcSize(new GUIContent(labelText)).x;
        }
    }
}
