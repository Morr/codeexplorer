﻿namespace CodeExplorer
{
    public enum Stereotype
    {
        Enum,
        Struct,
        Interface,
        Class
    }
}
