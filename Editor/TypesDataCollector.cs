using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace CodeExplorer
{
    public class TypesDataCollector : MonoBehaviour
    {
        private const string SEARCH_FOLDER = "Assets";
        private const string ASSET_TYPE_FILTER = "t:Script";
        private const string ASSEMBLY_QUALIFIER = "Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null";

        public Dictionary<Type, int> TypeScriptAssetInstanceIdMap { get; set; } = new Dictionary<Type, int>();
        public List<string> Namespaces { get; set; } = new List<string>();

        private Regex TypeRegex { get; set; } =  new Regex(@"(?<=\b(class|interface|struct|enum)\s+)\p{L}+");
        private Regex NamespaceRegex { get; set; } = new Regex(@"(?<=\bnamespace\s+)[\p{L}.]+");

        public TypesDataCollector()
        {
            CollectTypesData();
        }

        private void CollectTypesData()
        {
            string[] scriptAssetsGUIDs = GetScriptAssetsGUIDs();

            for (int i = 0; i < scriptAssetsGUIDs.Length; i++)
            {
                string scriptAssetGUID = scriptAssetsGUIDs[i];
                ProcessScriptAsset(scriptAssetGUID);
            }

            EditorUtility.UnloadUnusedAssetsImmediate();
        }

        private string[] GetScriptAssetsGUIDs()
        {
            return AssetDatabase.FindAssets(ASSET_TYPE_FILTER, new string[] { SEARCH_FOLDER });
        }

        private void ProcessScriptAsset(string scriptGUID)
        {
            string scriptPath = AssetDatabase.GUIDToAssetPath(scriptGUID);
            TextAsset scriptAsset = AssetDatabase.LoadAssetAtPath<TextAsset>(scriptPath);
            string fullTypeName = ExtractName(scriptAsset.text, TypeRegex);
            string namespaceName = ExtractName(scriptAsset.text, NamespaceRegex);

            if (namespaceName != null)
            {
                fullTypeName = $"{namespaceName}.{fullTypeName}";
            }

            if (fullTypeName != null)
            {
                Type type = Type.GetType($"{fullTypeName}, {ASSEMBLY_QUALIFIER}");
                TypeScriptAssetInstanceIdMap.Add(type, scriptAsset.GetInstanceID());

                if (Namespaces.Contains(type.Namespace) == false)
                {
                    Namespaces.Add(type.Namespace);
                }
            }
        }

        private string ExtractName(string scriptContent, Regex regexp)
        {
            MatchCollection classMatches = regexp.Matches(scriptContent);
            if (classMatches.Count > 0)
            {
                return classMatches[0].Value;
            }

            return null;
        }
    }
}
