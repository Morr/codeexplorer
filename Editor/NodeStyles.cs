using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace CodeExplorer
{
    public class NodeStyles : MonoBehaviour
    {
        public const float DEFAULT_NODES_HORIZONTAL_SPACING = 5.0f;
        public const float MIN_NODE_WIDTH = 250.0f;
        public const float NODE_BACKGROUND_OFFSET = 6.0f;
        public const float NODE_SEPARATOR_HEIGHT = 14.0f;
        public const float NODE_TITLE_HORIZONTAL_PADDING = 25.0f;
        public const int NODE_ENTRY_LEFT_PADDING = 10;
        public const string TYPE_COLOR = "#999999FF";

        public static GUIStyle NodeBoxStyle { get; private set; }
        public static GUIStyle NodeTitleStyle { get; private set; }
        public static GUIStyle NodeStereotypeStyle { get; private set; }
        public static GUIStyle NodeEntryStyle { get; private set; }

        private static Texture2D cached;

        static NodeStyles()
        {
            CreateStyles();
        }

        private static void CreateStyles()
        {
            NodeBoxStyle = new GUIStyle();
            cached = MakeTex(2, 2, new Color(0.3f, 0.31f, 0.32f, 1.0f) * 0.95f);
            NodeBoxStyle.normal.background = cached;
            //NodeBoxStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node0.png") as Texture2D;
            NodeBoxStyle.border = new RectOffset(12, 12, 12, 12);

            NodeTitleStyle = new GUIStyle();
            NodeTitleStyle.fontSize = 18;
            NodeTitleStyle.normal.textColor = Color.white;
            NodeTitleStyle.alignment = TextAnchor.MiddleCenter;

            NodeStereotypeStyle = new GUIStyle();
            NodeStereotypeStyle.fontSize = 13;
            NodeStereotypeStyle.alignment = TextAnchor.MiddleCenter;

            NodeEntryStyle = new GUIStyle();
            NodeEntryStyle.fontSize = 13;
            NodeEntryStyle.normal.textColor = Color.white;
            NodeEntryStyle.alignment = TextAnchor.MiddleLeft;
            NodeEntryStyle.padding = new RectOffset(NODE_ENTRY_LEFT_PADDING, 0, 0, 0);
            NodeEntryStyle.richText = true;
        }

        private static Texture2D MakeTex(int width, int height, Color col)
        {
            Color[] pix = new Color[width * height];

            for (int i = 0; i < pix.Length; ++i)
            {
                pix[i] = col;
            }

            Texture2D result = new Texture2D(width, height);
            result.SetPixels(pix);
            result.Apply();
            return result;
        }
    }
}

